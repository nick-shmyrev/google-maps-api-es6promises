# INFO3069 Web Applications & Technologies - Project 1

Uses ES6 syntax, tested in Chrome Version 61

This app uses Goggle Maps JS Geolocation, Places and Routing APIs. It finds the user's current location via Geolocation API, searches for Google Places of type 'bar' using Places API and displays the locations found no further than 3.5km of user's position. User can click the location icon to see the details and add the location to the list of waypoints. The list of waypoints will be used by Routing API to create a route. Locations then can be removed from the list, and the route will be recalculated using updated list of locations.

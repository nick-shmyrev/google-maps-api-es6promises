var map;
var userPos;
var service;
var placesArray = [];
var markersArray = [];
var infoWindow;
var directionsService;
var DirectionsRenderer;
var waypointsCoordsArray = [];
var waypointsDisplayArray = [];
var waypointsList = document.getElementById('waypoints-list');


/*
██ ███    ██ ██ ████████     ███████ ███████  ██████ ████████ ██  ██████  ███    ██
██ ████   ██ ██    ██        ██      ██      ██         ██    ██ ██    ██ ████   ██
██ ██ ██  ██ ██    ██        ███████ █████   ██         ██    ██ ██    ██ ██ ██  ██
██ ██  ██ ██ ██    ██             ██ ██      ██         ██    ██ ██    ██ ██  ██ ██
██ ██   ████ ██    ██        ███████ ███████  ██████    ██    ██  ██████  ██   ████
*/

function initMap() {

  // map options
  var options = {
    center: {lat: 42.9837, lng: -81.2497},
    zoom: 13,
    disableDefaultUI: true
  };

  map = new google.maps.Map(document.getElementById('map'), options);
  service = new google.maps.places.PlacesService(map);
  infoWindow = new google.maps.InfoWindow();
  directionsService = new google.maps.DirectionsService();
  DirectionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true,
                                                          polylineOptions: {
                                                            strokeColor: "#50893F",
                                                            strokeWeight: 5
                                                          }
                                                        });
}

// This section uses ES6 Promises, see: https://goo.gl/zS9wwm
getUserPos({
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
}).then((position) => {
  userPos = position;
  infoWindow.setPosition({lat: userPos.coords.latitude, lng: userPos.coords.longitude});
  infoWindow.setContent('You are here.');
  infoWindow.open(map);
  map.setCenter({
    lat: userPos.coords.latitude,
    lng: userPos.coords.longitude
  });
}).then(() => {
  return findPlaces({
    location: {
      lat: userPos.coords.latitude,
      lng: userPos.coords.longitude
    },
    radius: 3500,
    name: "pub",
    type: ['bar']
  });
}).then((results) => {
  placePins(results, markersArray);
  clusterMarkers(map, markersArray, {
    styles: [
      {url: 'img/beer-icon-cluster.svg', width: 48, height: 50}
    ]});
}).catch((error) => {
  console.log(`Error: ${error.message}`);
});




/*
███    ███ ███████ ████████ ██   ██  ██████  ██████  ███████
████  ████ ██         ██    ██   ██ ██    ██ ██   ██ ██
██ ████ ██ █████      ██    ███████ ██    ██ ██   ██ ███████
██  ██  ██ ██         ██    ██   ██ ██    ██ ██   ██      ██
██      ██ ███████    ██    ██   ██  ██████  ██████  ███████
*/


/**
 * Takes options object for geolocation, returns a Promise of getCurrentPosition() method
 * @method getUserPos
 * @param  {object}   options - Options object for the navigator.geolocation.getCurrentPosition() method
 *                              See W3C Geolocation API docs for details: https://goo.gl/jNqyst
 * @return {Promise}          - Returns a Promise of navigator.geolocation.getCurrentPosition()
 */
function getUserPos(options) {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    } else {
      reject(new Error('Geolocation not supported'));
    }
  });
} // END getUserPos()


/**
 * Takes request object as a parameter, returns a Promise of google nearbySearch() method
 * @method findPlaces
 * @param  {object}   request - Request object for the nearbySearch() method
 *                              See Google Maps JS API docs for details: https://goo.gl/ib2abq
 * @return {Promise}          - Returns a Promise of nearbySearch() method
 */
function findPlaces(request) {
  return new Promise((resolve, reject) => {
    service.nearbySearch(request, callback);
    function callback(results, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        placesArray = results;
        resolve(results);
      } else {
        reject(new Error(`Places API Error: ${status}`));
      }
    }
  });
} // END findPlaces()


/**
 * Takes an array of locations, creates map pins & generates infoWindows for locations.
 * @method placePins
 * @param  {array}  locationsArray - An array of Google Places API locations to show on the map
 * @param  {array}  markersArray   - [OPTIONAL] An array to store placed pins
 */
function placePins(locationsArray, markersArray) {
  for (var i = 0; i < locationsArray.length; i++) {
    createMarker(locationsArray[i], i);
  }
  function createMarker(location, locationIndex) {
    var marker = new google.maps.Marker({
      map: map,
      position: location.geometry.location,
      icon: 'img/beer-icon.svg',
      title: location.name,
      animation: google.maps.Animation.DROP
    });
    var content = `<h2 class='center-text'>${location.name}</h2>
                  <img class='center-block' src='${location.photos[0].getUrl({'maxWidth': 150, 'maxHeight': 150})}'></img>
                  <p class='center-text'><strong>Rating:</strong> ${location.rating} / 5</p>
                  <button class='center-block' type='button' onclick='addWaypoint(${locationIndex})'>Add to my crawl!</button>`;

    if (markersArray) {markersArray.push(marker);}

    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(content);
      infoWindow.open(map, this);
    });
  }
} // END placePins()


/**
 * A wrapper function around MarkerClusterer constructor.
 * @method clusterMarkers
 * @param  {object}       map          - Reference to Google Maps API map object
 * @param  {array}       markersArray  - Array of markers to cluster
 * @param  {object}       options      - Options object. See https://goo.gl/d8Ht6S for details.
 */
function clusterMarkers(map, markersArray, options) {
  new MarkerClusterer(map, markersArray, options);
} // END clusterMarkers()


/**
 * Adds the waypoint to the array used to display waypoints in the nav. menu and to the array used to calculate the route.
 * @method addWaypoint
 * @param  {integer}    placeIndex - Number of the waypoint to be added to arrays
 */
function addWaypoint(placeIndex) {
  var location = {
    lat: markersArray[placeIndex].internalPosition.lat(),
    lng: markersArray[placeIndex].internalPosition.lng()
  };
  /*
    This block of code makes sure user won't add the same location twice in a row.
    Because generating a route where you'd visit the same bar multiple times in a row won't make any sense.
    But going back and forth between two bars, while not very creative, does make sense.
    Max number of waypoints is limited to 23, see https://goo.gl/GtyNvt
  */
  if ((waypointsCoordsArray.length === 0) ||
      (waypointsCoordsArray.length > 0 && waypointsCoordsArray.length < 24 &&
        waypointsCoordsArray[waypointsCoordsArray.length - 1].location.lat !== location.lat &&
        waypointsCoordsArray[waypointsCoordsArray.length - 1].location.lng !== location.lng)
      ) {
    waypointsCoordsArray.push( {location: location, stopover: true} );
    waypointsDisplayArray.push({name: placesArray[placeIndex].name,
                            photoUrl: placesArray[placeIndex].photos[0].getUrl({'maxWidth': 100, 'maxHeight': 100})} );

    refreshWaypointsList();
  }
  // Updates route to display the new waypoint
  getRoute(directionsService, DirectionsRenderer);
} // END addWaypoint()


/**
 * Removes waypoint from route and from the menu
 * @method removeWaypoint
 * @param  {integer}       index - Number of an element to remove
 */
function removeWaypoint(index) {
  waypointsCoordsArray.splice(index, 1);
  waypointsDisplayArray.splice(index, 1);
  refreshWaypointsList();
  if (waypointsCoordsArray.length > 0) {
    getRoute(directionsService, DirectionsRenderer);
  } else {
    DirectionsRenderer.setMap(null);
  }
} // END removeWaypoint()


/**
 * Refreshes and generates the list of waypoints
 * @method refreshWaypointsList
 */
function refreshWaypointsList() {
  waypointsList.innerHTML = "";
  var index = 0;
  waypointsDisplayArray.forEach((location) => {
    // Create the DOM elements.
    var wpDiv = document.createElement("div");
    var wpHeader = document.createElement("h4");
    var wpImage = document.createElement("img");
    var wpRemove = document.createElement("button");

    // Set the content of elements.
    wpHeader.innerHTML = location.name;
    wpRemove.innerHTML = 'X';

    wpDiv.setAttribute("class", "waypoints-list-item");
    wpImage.setAttribute("src", location.photoUrl );
    wpRemove.setAttribute("type", "button");
    wpRemove.setAttribute("title", "Remove item");
    wpRemove.setAttribute("onclick", `removeWaypoint(${index++});`);

    // Assemble the list item.
    wpDiv.appendChild(wpHeader);
    wpDiv.appendChild(wpImage);
    wpDiv.appendChild(wpRemove);

    // Add the list item to the container.
    waypointsList.appendChild(wpDiv);
  });
} // END refreshWaypointsList()


/**
 * Generates a route between user's current location and the last location in the waypoins[].
 * If there are more than 1 location in the waypoins[], those will be used as waypoints.
 * @method getRoute
 * @param  {object} directionsService - Google Maps JS API directionsService object. See https://goo.gl/NnVpBP
 * @param  {object} DirectionsRenderer - Google Maps JS API DirectionsRenderer object. See https://goo.gl/mhpVxg
 */
function getRoute(directionsService, DirectionsRenderer) {
  let origin = {lat:userPos.coords.latitude, lng: userPos.coords.longitude};
  let destination = {lat: waypointsCoordsArray[waypointsCoordsArray.length - 1].location.lat,
                     lng: waypointsCoordsArray[waypointsCoordsArray.length - 1].location.lng};

  directionsService.route({
    origin: origin,
    destination: destination,
    waypoints: waypointsCoordsArray,
    travelMode: 'WALKING'
  }, function(response, status) {
    if (status === 'OK') {
      DirectionsRenderer.setDirections(response);
      infoWindow.close(map);
    } else {
      window.alert(`Directions request failed due to ${status}.`);
    }
  });
  DirectionsRenderer.setMap(map);
} // END getRoute()
